<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\RegisterFormRequest;
use App\Models\User;
use JWTAuth;

class AuthController extends Controller
{
    /**
     * Register user.
     *
     * @param  \Illuminate\Http\RegisterFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public static function register(RegisterFormRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->save();
        return response(
            [
                'status' => 'success',
                'data' => $user
            ],
            200
        );
    }

    /**
     * Login user by JWTAuth.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!$token = JWTAuth::attempt($credentials)) {
            return response(
                [
                    'status' => 'error',
                    'error' => 'invalid.credentials',
                    'msg' => 'Invalid Credentials.'
                ],
                400
            );
        }
        return response(
            [
                'status' => 'success'
            ]
        )
            ->header('Authorization', $token);
    }

    /**
     * Get user for refresh.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function user(Request $request)
    {
        $user = User::find(Auth::user()->id);
        return response(
            [
                'status' => 'success',
                'data' => $user
            ]
        );
    }

    /**
     * Refresh user.
     *
     * @return \Illuminate\Http\Response
     */
    public function refresh()
    {
        return response(
            [
                'status' => 'success'
            ]
        );
    }

    /**
     * Log out current user.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        JWTAuth::invalidate();
        return response(
            [
                'status' => 'success',
                'msg' => 'Logged out Successfully.'
            ],
            200
        );
    }
}
