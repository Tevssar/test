<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'name' => [
                'required',
                'string',
                 Rule::unique('users')->ignore($this->get('id'))
            ],
            'email' => [
                'required',
                'email',
                 Rule::unique('users')->ignore($this->get('id'))
            ],
        ];

        if ($this->getMethod() == 'POST') {
            $rules += ['password' => 'required|string|min:6|max:100'];
        }

        return $rules;
    }
}
