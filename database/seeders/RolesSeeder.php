<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   $date = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('roles')->insert(
            [
            'name' => 'user',
            'created_at' => $date,
            'updated_at' => $date
            ]
        );
        DB::table('roles')->insert(
            [
            'name' => 'admin',
            'created_at' => $date,
            'updated_at' => $date
            ]
        );

    }
}
