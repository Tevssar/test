(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Users.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Users.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-multiselect */ "./node_modules/vue-multiselect/dist/vue-multiselect.min.js");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_multiselect__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/**
 * Catalog component
 */

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Users",
  components: {
    Multiselect: vue_multiselect__WEBPACK_IMPORTED_MODULE_1___default.a
  },

  /**
   * Users and filters data
   * @vue-data {Array} activeRoles - Currently active filters
   * @vue-data {Array} users - List of all users
   * @vue-data {Array} filters - List of all filters
   * @vue-data {Array} selectedUser - Currently selected user
   * @vue-data {Boolean} success - User updade status
   * @vue-data {Boolean} error - User updade error
   * @vue-data {Array} errors - User updade error list
   * @vue-data {setTimeout} timer - User updade fade timer
   */
  data: function data() {
    return {
      activeRoles: [],
      users: [],
      filters: [],
      selectedUser: [],
      success: false,
      error: false,
      errors: [],
      timer: null
    };
  },
  methods: {
    /**
     * Get users from API
     */
    getUsers: function getUsers() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return axios.get("users");

              case 2:
                response = _context.sent;
                _this.users = response.data;
                console.log(_this.users);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },

    /**
     * Get filters from API
     */
    getRoles: function getRoles() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return axios.get("roles");

              case 2:
                response = _context2.sent;
                _this2.filters = response.data;

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },

    /**
     * Delete user using API
     * @param {int} id
     */
    deletUser: function deletUser(id) {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (!confirm("Do you really want to delete?")) {
                  _context3.next = 4;
                  break;
                }

                _context3.next = 3;
                return axios["delete"]("users/" + id);

              case 3:
                _this3.getUsers();

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },

    /**
     * Update user using API
     * @param {array} user
     */
    updateUser: function updateUser(user) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (user.length !== 0) {
                  axios.put("users/" + user.id, user).then(function (res) {
                    var index = _this4.users.findIndex(function (x) {
                      return x.id === user.id;
                    });

                    _this4.users[index] = user;
                    clearTimeout(_this4.timer);
                    _this4.success = true;
                    _this4.timer = setTimeout(function () {
                      _this4.success = false;
                    }, 2000);
                  }, function (res) {
                    clearTimeout(_this4.timer);
                    _this4.error = true;
                    _this4.errors = res.response.data.errors;
                    _this4.timer = setTimeout(function () {
                      _this4.error = false;
                    }, 2000);
                  });
                }

              case 1:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },

    /**
     * Set string format for <multiselect ... > ... </multiselect> items
     * @param {String} title
     * @param {String} id
     * @returns {String}
     */
    TitleAndId: function TitleAndId(_ref) {
      var name = _ref.name,
          id = _ref.id;
      return "".concat(name, " \u2014 [id:").concat(id, "]");
    },

    /**
     * Filter all listed users by active filters
     * @param {Array} users
     * @returns {Array}
     */
    filterUsers: function filterUsers(users) {
      var _this5 = this;

      return users.filter(function (user) {
        if (_this5.activeRoles.length === 0) return true;
        var result = false;
        $.each(_this5.activeRoles, function (key, value) {
          if (user.roles.id == value.id) {
            result = true;
            return true;
          }
        });
        return result;
      });
    }
  },

  /**
   * When mounted get all users and filters
   */
  mounted: function mounted() {
    this.getUsers();
    this.getRoles();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Users.vue?vue&type=template&id=30c27aa6&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/Users.vue?vue&type=template&id=30c27aa6& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "container", attrs: { id: "usersComponent" } },
    [
      _c("label", { staticClass: "h4" }, [_vm._v("filters")]),
      _vm._v(" "),
      _c("div", { staticClass: "row position-relative" }, [
        _c(
          "div",
          { staticClass: "col-8 position-absolute left-0 right-0 m-auto" },
          [
            _c("multiselect", {
              attrs: {
                options: _vm.filters,
                multiple: true,
                "close-on-select": false,
                "clear-on-select": false,
                "preserve-search": true,
                placeholder: "Pick some",
                "custom-label": _vm.TitleAndId,
                label: "name",
                "track-by": "name",
                "preselect-first": false
              },
              scopedSlots: _vm._u([
                {
                  key: "selection",
                  fn: function(ref) {
                    var values = ref.values
                    var search = ref.search
                    var isOpen = ref.isOpen
                    return [
                      values.length && !isOpen
                        ? _c("span", { staticClass: "multiselect__single" }, [
                            _vm._v(_vm._s(values.length) + " options selected")
                          ])
                        : _vm._e()
                    ]
                  }
                }
              ]),
              model: {
                value: _vm.activeRoles,
                callback: function($$v) {
                  _vm.activeRoles = $$v
                },
                expression: "activeRoles"
              }
            })
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c("label", { staticClass: "h4" }, [_vm._v("users")]),
      _vm._v(" "),
      _c("div", { staticClass: "row pr-5 pl-5" }, [
        _c(
          "div",
          { staticClass: "list-group col-6 overflow-auto" },
          _vm._l(_vm.filterUsers(_vm.users), function(user) {
            return _c(
              "a",
              {
                key: user.id,
                staticClass: "list-group-item list-group-item-action",
                on: {
                  click: function($event) {
                    _vm.selectedUser = Object.assign({}, user)
                  }
                }
              },
              [
                _vm._v("\n        id:" + _vm._s(user.id) + "\n        "),
                _c(
                  "button",
                  {
                    staticClass: "btn btn-sm btn-danger btn-delete",
                    on: {
                      click: function($event) {
                        return _vm.deletUser(user.id)
                      }
                    }
                  },
                  [_vm._v("\n          x\n        ")]
                ),
                _vm._v(" "),
                _c("br"),
                _vm._v("\n        name:" + _vm._s(user.name) + "\n        "),
                _c("br"),
                _vm._v("\n        email:" + _vm._s(user.email) + "\n      ")
              ]
            )
          }),
          0
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "col-6" },
          [
            _c("label", { staticClass: "h6" }, [_vm._v("Name")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.selectedUser.name,
                  expression: "selectedUser.name"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text" },
              domProps: { value: _vm.selectedUser.name },
              on: {
                blur: function($event) {
                  return _vm.updateUser(_vm.selectedUser)
                },
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.selectedUser, "name", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("transition", { attrs: { name: "fade" } }, [
              _vm.error && _vm.errors.name
                ? _c("div", { staticClass: "alert alert-danger" }, [
                    _vm._v(
                      "\n          " + _vm._s(_vm.errors.name[0]) + "\n        "
                    )
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("label", { staticClass: "h6" }, [_vm._v("Email")]),
            _vm._v(" "),
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.selectedUser.email,
                  expression: "selectedUser.email"
                }
              ],
              staticClass: "form-control mt-2",
              attrs: { type: "text" },
              domProps: { value: _vm.selectedUser.email },
              on: {
                blur: function($event) {
                  return _vm.updateUser(_vm.selectedUser)
                },
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.selectedUser, "email", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _c("transition", { attrs: { name: "fade" } }, [
              _vm.error && _vm.errors.email
                ? _c("div", { staticClass: "alert alert-danger" }, [
                    _vm._v(
                      "\n          " +
                        _vm._s(_vm.errors.email[0]) +
                        "\n        "
                    )
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c("transition", { attrs: { name: "fade" } }, [
              _vm.success
                ? _c("div", { staticClass: "alert alert-success" }, [
                    _c("p", [_vm._v("Update completed.")])
                  ])
                : _vm._e()
            ])
          ],
          1
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/Users.vue":
/*!*******************************************!*\
  !*** ./resources/js/components/Users.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Users_vue_vue_type_template_id_30c27aa6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Users.vue?vue&type=template&id=30c27aa6& */ "./resources/js/components/Users.vue?vue&type=template&id=30c27aa6&");
/* harmony import */ var _Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Users.vue?vue&type=script&lang=js& */ "./resources/js/components/Users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Users_vue_vue_type_template_id_30c27aa6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Users_vue_vue_type_template_id_30c27aa6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Users.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/Users.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/components/Users.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Users.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Users.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/Users.vue?vue&type=template&id=30c27aa6&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/Users.vue?vue&type=template&id=30c27aa6& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_30c27aa6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Users.vue?vue&type=template&id=30c27aa6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/Users.vue?vue&type=template&id=30c27aa6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_30c27aa6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Users_vue_vue_type_template_id_30c27aa6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);