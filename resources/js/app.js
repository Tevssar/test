/**
 * Vue applicaton
 */

window.Vue = require('vue');

import './bootstrap';
import AppComponent from './components/App'
import router from './router'
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import auth from '@websanova/vue-auth'
import authBearer from '@websanova/vue-auth/drivers/auth/bearer.js'
import authAxios from '@websanova/vue-auth/drivers/http/axios.1.x.js'
import authRouter from '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.esm.js';

axios.defaults.baseURL = process.env.MIX_API_URL+'/api/v01';
Vue.router = router

Vue.use(VueAxios, axios, VueRouter);

Vue.use(
    auth,
    {
        auth: authBearer,
        http: authAxios,
        router: authRouter,
        rolesKey: 'role',
    }
);


const app = new Vue({
    el: '#app',
    router,
    template: '<AppComponent/>',
    components: { AppComponent }
});
