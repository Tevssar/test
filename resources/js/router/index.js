
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'Users',
            component: () => import('./../components/Users'),
            meta: {
                auth: true
            }
        },
        {
            path: '/register',
            name: 'register',
            component: () => import('./../components/Register'),
            meta: {
                auth: false
            }
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./../components/Login'),
            meta: {
                auth: false
            }
        }

    ]
})
