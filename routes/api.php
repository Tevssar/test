<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Users;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get(
    '/user',
    function (Request $request) {
        return $request->user();
    }
);
Route::group(
    ['prefix' => 'v01', 'middleware' => 'api'],
    function () {
        Route::post('auth/register', [AuthController::class, 'register']);
        Route::post('auth/login', [AuthController::class, 'login']);
        Route::group(
            ['middleware' => 'jwt.auth'],
            function () {
                Route::get('auth/user', [AuthController::class, 'user']);
                Route::post('auth/logout', [AuthController::class, 'logout']);
                Route::get('/roles', [Users::class, 'roles']);
                Route::group(
                    ['prefix' => 'users'],
                    function () {
                        Route::get('/', [Users::class, 'index']);
                        Route::get('/{user}', [Users::class, 'show']);
                        Route::post('/', [Users::class, 'store']);
                        Route::put('/{id}', [Users::class, 'update']);
                        Route::delete('/{user}', [Users::class, 'destroy']);

                    }
                );
            }
        );
        Route::group(
            ['middleware' => 'jwt.refresh'],
            function () {
                Route::get('auth/refresh', [AuthController::class, 'refresh']);
            }
        );
    }
);
